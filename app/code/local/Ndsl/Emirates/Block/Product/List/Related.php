<?php
class Ndsl_Emirates_Block_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{

    protected function calculateEmi($price,$int_rate,$emi_months){

        $int_rate = $int_rate/(12*100);
        $nominator =$price * $int_rate * pow((1+$int_rate),$emi_months);
        $denominator = pow((1+$int_rate),($emi_months))-1;
        $emi = $nominator/$denominator;
        $emi = number_format($emi,0);

        return $emi;
    }



}