<?php
class Ndsl_QueryForm_IndexController extends Mage_Core_Controller_Front_Action
{
    const XML_PATH_EMAIL_RECIPIENT  = 'contacts/email/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'contacts/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'contacts/email/email_template';
    const XML_PATH_ENABLED          = 'contacts/contacts/enabled';
    protected function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function indexAction(){

        $session = $this->_getSession();

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Contact Form"));
        /*$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("customer group login", array(
            "label" => $this->__("Customer Group Login"),
            "title" => $this->__("Customer Group Login")
        ));
		*/
        $this->loadLayout();

        $this->renderLayout();

    }

    Public function subformAction(){

        // For accessing the all data
        $query_type= $_POST['field']['1540'];
        $general_subject= $_POST['field']['1541'];
        $order_related= $_POST['field']['1542'];
        $name= $_POST['field']['1543'];
        $email= $_POST['field']['1544'];
        $number= $_POST['field']['1545'];
        $order_number= $_POST['field']['1546'];
        $location= $_POST['field']['1547'];
        $pincode= $_POST['field']['1548'];
        $coupon_code= $_POST['field']['1550'];
        $product_name= $_POST['field']['1551'];
        $product_qty= $_POST['field']['1552'];
        $additional= $_POST['field']['1553'];

        // For subject of email
        if($query_type=="Order Related")
        {
            $sub=$order_related;
        }
        else
        {
            if($query_type=="General Enquiry")
            {
                $sub=$general_subject;
            }

        }

        // For message as per the input given by user
        $message='  ';
        if($name!="")
        {
            $message.="Name : ".$name."<br/>";
        }
        if($email!="") {
            $message .= "Email : " . $email . "<br/>";
        }
        if($number!="") {
            $message .= "Number : " . $number . "<br/>";
        }
        if($order_number!="") {
            $message .= "Order Number : " . $order_number . "<br/>";
        }
        if($location!="") {
            $message .= "Location : " . $location . "<br/>";
        }
        if($pincode!="") {
            $message .= "Pincode : " . $pincode . "<br/>";
        }
        if($coupon_code!="") {
            $message .= "Coupon Code : " . $coupon_code . "<br/>";
        }
        if($product_name!="") {
            $message .= "Product Name : " . $product_name . "<br/>";
        }
        if($product_qty!="") {
            $message .= "Product Quantity : " . $product_qty . "<br/>";
        }
        if($additional!="") {
            $message .= "Additional Comment : " . $additional . "<br/>";
        }

        // For mail
        $recipient = Mage::getStoreConfig('trans_email/ident_support/email');

        $mail_to = $recipient;
        // $from_mail = Mage::getStoreConfig('trans_email/ident_general/email');
        $from_mail = $email;
        // $from_name = Mage::getStoreConfig('trans_email/ident_general/name');
        $from_name = $name;
        $reply_to = $email;
        $directory_path=Mage::getBaseDir();
        // if for query with image upload else for remaining else
        if($order_related=="Exchange related" || $order_related=="Defective Product" || $order_related=="Physically Damaged Product") {
            if($_FILES['file_1549']['name']!="") {
                defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
                define('BASE_DIR', $directory_path);
                require_once(BASE_DIR . '/app/Mage.php');
                ini_set("display_errors", 1);
                set_time_limit(0);
                umask(0);
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                $path = $_FILES['file_1549']['name'];
                $tmp_name = $_FILES['file_1549']['tmp_name'];
                $name = BASE_DIR . DS . $_FILES['file_1549']['name'];
                move_uploaded_file($tmp_name, $name);
                $file_name = $_FILES['file_1549']['name'];
                $path = BASE_DIR . DS;

                // Read the file content
                $file = BASE_DIR . DS . $_FILES['file_1549']['name'];
                $file_size = filesize($file);
                $handle = fopen($file, "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $content = chunk_split(base64_encode($content));

                /* Set the email header */
                // Generate a boundary
                $boundary = md5(uniqid(time()));

                // Email header
                $header = "From: " . $from_name . " <" . $from_mail . ">" . PHP_EOL;
                $header .= "Reply-To: " . $email . PHP_EOL;
                $header .= "MIME-Version: 1.0" . PHP_EOL;

                // Multipart wraps the Email Content and Attachment
                $header .= "Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"" . PHP_EOL;
                $header .= "This is a multi-part message in MIME format." . PHP_EOL;
                $header .= "--" . $boundary . PHP_EOL;

                // Email content
                // Content-type can be text/plain or text/html
                $header .= "Content-type:text/html; charset=iso-8859-1" . PHP_EOL;
                $header .= "Content-Transfer-Encoding: 7bit" . PHP_EOL . PHP_EOL;
                $header .= "$message" . PHP_EOL;
                $header .= "--" . $boundary . PHP_EOL;

                // Attachment
                // Edit content type for different file extensions
                $header .= "Content-Type: application/xml; name=\"" . $file_name . "\"" . PHP_EOL;
                $header .= "Content-Transfer-Encoding: base64" . PHP_EOL;
                $header .= "Content-Disposition: attachment; filename=\"" . $file_name . "\"" . PHP_EOL . PHP_EOL;
                $header .= $content . PHP_EOL;
                $header .= "--" . $boundary . "--";
            }
            else
            {
                $boundary = md5(uniqid(time()));
                $header = "From: ".$from_name." <".$from_mail.">".PHP_EOL;
                $header .= "Reply-To: ".$email.PHP_EOL;
                $header .= "MIME-Version: 1.0".PHP_EOL;
                $header .= "Content-type:text/html; charset=iso-8859-1".PHP_EOL;
                $header .= "Content-Transfer-Encoding: 7bit".PHP_EOL.PHP_EOL;
            }
            // Send email
            if (mail($mail_to, $sub, $message, $header)) {
                Mage::getSingleton('customer/session')->addSuccess("Thank you for contacting us. We'll get in touch with you soon.");
            } else {
                Mage::getSingleton('customer/session')->addError("There was an issue submitting the form,please try again later.
");
            }

            // Remove uploaded image from base directory
            if($_FILES['file_1549']['name']!="") {
                array_map('unlink', glob(BASE_DIR . DS . $_FILES['file_1549']['name']));
            }
        }
        else
        {
            $boundary = md5(uniqid(time()));
            $header = "From: ".$from_name." <".$from_mail.">".PHP_EOL;
            $header .= "Reply-To: ".$email.PHP_EOL;
            $header .= "MIME-Version: 1.0".PHP_EOL;
            $header .= "Content-type:text/html; charset=iso-8859-1".PHP_EOL;
            $header .= "Content-Transfer-Encoding: 7bit".PHP_EOL.PHP_EOL;


            if (mail($mail_to, $sub, $message, $header)) {
                Mage::getSingleton('customer/session')->addSuccess("Thank you for contacting us. We'll get in touch with you soon.");
            } else {
                Mage::getSingleton('customer/session')->addError("There was an issue submitting the form,please try again later.");
            }
        }



        $this->_redirect('contact-us/');


    }

}


?>