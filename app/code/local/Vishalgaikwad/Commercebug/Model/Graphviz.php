<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
class Vishalgaikwad_Commercebug_Model_Graphviz
{
    public function capture()
    {    
        $collector  = new Vishalgaikwad_Commercebug_Model_Collectorgraphviz; 
        $o = new stdClass();
        $o->dot = Vishalgaikwad_Commercebug_Model_Observer_Dot::renderGraph();
        $collector->collectInformation($o);
    }
    
    public function getShim()
    {
        $shim = Vishalgaikwad_Commercebug_Model_Shim::getInstance();        
        return $shim;
    }    
}