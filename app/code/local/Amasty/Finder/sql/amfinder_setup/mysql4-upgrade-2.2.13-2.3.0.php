<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Finder
 */


$this->startSetup();

$this->run("
ALTER TABLE `{$this->getTable('amfinder/map')}` ADD INDEX(`pid`);
");

$this->run("
ALTER TABLE `{$this->getTable('amfinder/universal')}` ADD INDEX(`pid`);
");

$this->endSetup(); 
