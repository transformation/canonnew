<?php 
${"GLOBALS"}["stjpmhinjc"]="isAllowed";
${"GLOBALS"}["csymzxvhsgv"]="adminSession";
${"GLOBALS"}["yjjevugc"]="actionName";
${"GLOBALS"}["uvnlomhro"]="postcodeId";
${"GLOBALS"}["ftfphcnf"]="codeModel";
${"GLOBALS"}["srryouyshsc"]="postcodeIds";
${"GLOBALS"}["zwzglzurjml"]="contentType";
${"GLOBALS"}["sbonsuq"]="content";
${"GLOBALS"}["pygigqjclk"]="fileName";
${"GLOBALS"}["ljhsauiqx"]="postcodeEditBlock";
${"GLOBALS"}["mwvulxfygez"]="postData";
${"GLOBALS"}["vltqjju"]="postcode";
${"GLOBALS"}["cxpkdedjmuu"]="postcodeBlock";

class AsifHussain_ShippingCod_Adminhtml_PostcodeController extends Mage_Adminhtml_Controller_Action {	
	public function indexAction(){		
		${"GLOBALS"}["dkxfwvy"]="postcodeBlock";
		Mage::getSingleton("asifhussain_shippingcod/postcode")->_checkCodMod();
		${${"GLOBALS"}["dkxfwvy"]}=$this->getLayout()->createBlock("asifhussain_shippingcod_adminhtml/postcode");
		$this->loadLayout()->_addContent(${${"GLOBALS"}["cxpkdedjmuu"]})->renderLayout();
	}	
	public function editAction() {		
		${"GLOBALS"}["kmlvdde"]="postcodeId";
		${${"GLOBALS"}["vltqjju"]}=Mage::getModel("asifhussain_shippingcod/postcode");
		${"GLOBALS"}["ujudrsz"]="postcode";
		${"GLOBALS"}["chfzsjnu"]="postcodeId";
		if(${${"GLOBALS"}["chfzsjnu"]}=$this->getRequest()->getParam("id",false)) {			
			${"GLOBALS"}["ogrkoycil"]="postcodeId";
			$postcode->load(${${"GLOBALS"}["ogrkoycil"]});
		}		
		if(!$postcode->getId()&&${${"GLOBALS"}["kmlvdde"]}>0) {			
			$this->_getSession()->addError($this->__("This postcode no longer exists."));
			return $this->_redirect("asifhussain_shippingcod_admin/postcode/index");
		}		
		if(${${"GLOBALS"}["mwvulxfygez"]}=$this->getRequest()->getPost("postcodeData")) {
			try{
				$postcode->addData(${${"GLOBALS"}["mwvulxfygez"]});
				$postcode->save();
				$this->_getSession()->addSuccess($this->__("New postcode has been saved."));
			return $this->_redirect("asifhussain_shippingcod_admin/postcode/index");
			}catch(Exception$e) {
				${"GLOBALS"}["qmwnvmz"]="e";
				Mage::logException(${${"GLOBALS"}["qmwnvmz"]});
				$this->_getSession()->addError($e->getMessage());
			}
		}
		Mage::register("current_postcode",${${"GLOBALS"}["ujudrsz"]});
		${${"GLOBALS"}["ljhsauiqx"]}=$this->getLayout()->createBlock("asifhussain_shippingcod_adminhtml/postcode_edit");
		$this->loadLayout()->_addContent(${${"GLOBALS"}["ljhsauiqx"]})->renderLayout();
	}		
	public function exportCsvAction(){		
		${${"GLOBALS"}["pygigqjclk"]}="postcodes.csv";
		${${"GLOBALS"}["sbonsuq"]}=$this->getLayout()->createBlock("asifhussain_shippingcod_adminhtml/postcode_grid")->getCsvFile();
		$this->_prepareDownloadResponse(${${"GLOBALS"}["pygigqjclk"]},${${"GLOBALS"}["sbonsuq"]});
	}	
	public function exportExcelAction(){		
		$hnwycof="fileName";
		$cimkowxvlxrx="fileName";
		$jssmxvvtyd="grid";
		${$cimkowxvlxrx}="postcodes.xml";
		${$jssmxvvtyd}=$this->getLayout()->createBlock("asifhussain_shippingcod_adminhtml/postcode_grid");
		$this->_prepareDownloadResponse(${$hnwycof},$grid->getExcelFile(${${"GLOBALS"}["pygigqjclk"]}));
	}	
	protected function _sendUploadResponse($fileName,$content,$contentType='application/octet-stream'){		
		${"GLOBALS"}["proxofuchkk"]="fileName";
		${"GLOBALS"}["obpipciypv"]="content";
		$this->_prepareDownloadResponse(${${"GLOBALS"}["proxofuchkk"]},${${"GLOBALS"}["obpipciypv"]},${${"GLOBALS"}["zwzglzurjml"]});
	}	
	public function massDeleteAction(){		
		${${"GLOBALS"}["srryouyshsc"]}=$this->getRequest()->getParam("postcode_id");
		if(!is_array(${${"GLOBALS"}["srryouyshsc"]})){			
			Mage::getSingleton("adminhtml/session")->addError(Mage::helper("asifhussain_shippingcod")->__("Please select postcode(s)."));
		}else {
			try{				
				${"GLOBALS"}["kmpxxeeoh"]="postcodeIds";
				${${"GLOBALS"}["ftfphcnf"]}=Mage::getModel("asifhussain_shippingcod/postcode");
				foreach(${${"GLOBALS"}["kmpxxeeoh"]} as${${"GLOBALS"}["uvnlomhro"]}){
					${"GLOBALS"}["qqbxhimd"]="postcodeId";
					$codeModel->load(${${"GLOBALS"}["qqbxhimd"]})->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("asifhussain_shippingcod")->__("Total of %d postcode(s) were deleted.",count(${${"GLOBALS"}["srryouyshsc"]})));
			}catch(Exception$e){
				Mage::getSingleton("adminhtml/session")->
				addError($e->getMessage());
			}		
		}
		$this->_redirect("*/*/index");
	}	
	public function deleteAction(){		
		$wrubqerab="postcode";
		${"GLOBALS"}["jwrcfnbalay"]="postcodeId";
		${$wrubqerab}=Mage::getModel("asifhussain_shippingcod/postcode");
		if(${${"GLOBALS"}["jwrcfnbalay"]}=$this->getRequest()->getParam("id",false)){			
			$qgmtdd="postcodeId";
			$postcode->load(${$qgmtdd});
		}
		if(!$postcode->getId()) {
			$this->_getSession()->addError($this->__("This postcode no longer exists."));
		return $this->_redirect("asifhussain_shippingcod_admin/postcode/index");
		}try{
			$postcode->delete();
			$this->_getSession()->addSuccess($this->__("The postcode has been deleted."));
		}catch(Exception$e){			
			${"GLOBALS"}["ruqenrvxoyn"]="e";
			Mage::logException(${${"GLOBALS"}["ruqenrvxoyn"]});
			$this->_getSession()->addError($e->getMessage());
		}		
		return $this->_redirect("asifhussain_shippingcod_admin/postcode/index");
	}	
	protected function _isAllowed(){		
		$eybsnpbz="actionName";
		${${"GLOBALS"}["yjjevugc"]}=$this->getRequest()->getActionName();
		$dbqkldct="isAllowed";
		switch(${$eybsnpbz}){			
			case"index":
			case"edit":
			case"delete":
			default:
				${${"GLOBALS"}["csymzxvhsgv"]}=Mage::getSingleton("admin/session");
				${$dbqkldct}=$adminSession->isAllowed("asifhussain_shippingcod/postcode");
				break;
		}		
		return${${"GLOBALS"}["stjpmhinjc"]};
	}
}
?>